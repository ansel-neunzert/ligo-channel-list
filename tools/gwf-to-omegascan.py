#!/usr/bin/env python
usage       = "gwf-to-omegascan.py [--options] path/to/gwf"
description = "converts a gwf file into an OmegaScan config. Prints the resulting config file to stdout if --output-file is not specified.\nWe note that restrictions on parameters are taken from the Omega source code here : https://svn.ligo.caltech.edu/svn/omega/trunk/src/wtile.m"
author      = "reed.essick@ligo.org"

#-------------------------------------------------

import numpy as np

import os
import sys
import subprocess as sp

import re

import getpass

import time

from ConfigParser import SafeConfigParser

from optparse import OptionParser

#-------------------------------------------------

### convenient constants
sqrt11 = 11**0.5
sqrt2  = 2**0.5
twopi    = 2*np.pi

#-------------------------------------------------

def ceil( x, d=0 ):
    """
    take the ceiling of x at the "d^th" decimal point
    """
    fact = 10**d
    return np.ceil( x*fact ) / fact

def floor( x, d=0 ):
    """
    take the floor of x at the "d^th" decimal point
    """
    fact = 10**d
    return np.floor( x*fact ) / fact

def search_time_range( fmax ):
    """
    a glorified look-up table for the --search-time-range based on the maximum frequency.
    used when --search-time-range is not supplied by user
    """
    if fmax > 256:
        return 64.0

    elif fmax > 128:
        return 128.0

    elif fmax > 64:
        return 256.0

    else:
        return 1024.0

def search_window_duration( fmax ):
    """
    a glorified look-up table for the --search-window-duration based on the maximum frequency.
    used when --search-window-duration is not supplied by user
    """
    if fmax > 256:
        return 0.5

    else:
        return 1.0

def plot_time_range( fmax ):
    """
    a glorified look-up table for the --plot-time-range based on the maximum frequency.
    used when --plot-time-range is not supplied by user
    """
    if fmax > 64:
        return [1.0, 4.0, 16.0]

    else:
        return [8.0, 64.0, 512.0]

#------------------------

### define some useful methods

def gwf2chans( gwf ):
    '''
    extract and format channels and sampling frequencies from a gwf file
    '''
    proc = sp.Popen( ['FrChannels', gwf], stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = proc.communicate()

    if proc.returncode:
        raise RuntimeError( 'FrChannels returncode=%d\n%s\n%s'%(proc.returncode, out, err) )

    ### format into a dictionary with (channelName : samplingFreq) pairs
    chans = dict( line.strip().split() for line in out.strip().split('\n') )
    for key, val in chans.items():
        chans[key] = float(val)

    return chans

#-------------------------------------------------

parser = OptionParser(usage=usage, description=description)

### verbosity options
parser.add_option('-v', '--verbose', default=False, action='store_true',
    help='print information' )

parser.add_option('-V', '--Verbose', default=False, action='store_true', 
    help='print even more information' )

parser.add_option('-o', '--output-file', default=None, type='string', 
    help='path to new OmegaScan config. If not specified, config will be printed to stdout' )

### options about specific channels
parser.add_option('', '--channel-exclude', default=[], action='append', type='string',
    help='exclude this channel (requires exact match). Can be repeated' )

parser.add_option('', '--regex-channel-exclude', default=[], action='append', type='string',
    help='exclude all channels that match this regular expression. Can be repeated' )

### options for formatting the scan
parser.add_option('', '--f-low', default=4, type='float',
    help='the low frequency cuttoff used in search. DEFAULT=4. \
If supplied, this will be applied to all channels independent of the sampling frequencies. Will also overwrite anything specified \
with --freq-map' )

parser.add_option('', '--f-high', default=None, type='float',
    help='the maximum allowed frequency used in the search. DEFAULT=None and will be based off Nyquist.\
If supplied, this will be applied to all channels independent of the sampling frequencies. Will also overwrite anything specified \
with --freq-map' )

parser.add_option('', '--search-time-range', default=None, type='float',
    help='the time range (+/- search_time_range) through which the OmegaScan will look for triggers. \
DEFAULT=None and will follow simple logic based on fhigh. If supplied, will override this for all channels.' )

parser.add_option('', '--search-window-duration', default=None, type='float',
    help='the window (+/- search_window_duration/2) used to determine whether there was a coincident event.\
DEFAULT=None and will follow simple logic based on fhigh. If supplied, will override this for all channels.' )

parser.add_option('', '--q-low', default=4, type='float',
    help='the minimum q used within OmegaScans. DEFAULT=4. \
Note, Omega requires --q-low >= sqrt(11) and we inforce this (raise an error if this is violated)' )

parser.add_option('', '--q-high', default=100, type='float',
    help='the maximum q value used in search. DEFAULT=100.' )

parser.add_option('', '--max-energy-loss', default=0.2, type='float',
    help='the maximum energy loss allowed when placing templates. DEFAULT=0.2' )

parser.add_option('', '--white-noise-false-alarm-rate', default=1e-3, type='float',
    help='the white noise false alarm rate used within OmegaScan\'s search. DEFAULT=1e-3' )

parser.add_option('', '--plot-time-range', default=[], type='float', action='append',
    help='the time range used when generating plots. DEFAULT=[] and will follow simple logic based on fhigh.\
If specified, will override this for all channels; multiple time ranges can be specified by repeating this argument.' )

parser.add_option('', '--plot-frequency-range', default=[], type='float', action='append',
    help='the frequency ranges used when generating plots. DEFAULT=[]. Additional ranges can be specified\
by repeating this argument.' )

parser.add_option('', '--plot-energy-min', default=0, type='float', action='append',
    help='the mimimum energy used when setting the plot\'s color bar. DEFAULT=0' )

parser.add_option('', '--plot-energy-max', default=25, type='float', action='append',
    help='the maximum energy used when setting the plot\'s color bar. DEFAULT=25' )

parser.add_option('', '--always-plot-all-channels', default=False, action='store_true',
    help='a boolean flag determine whether we always plot a channel. DEFAULT=FALSE. Specifying this option\
sets the flag to True.' )

parser.add_option('', '--always-plot-channel', default=[], action='append', type='string',
    help='set --always-plot=True for this specific channel. Can be repeated for multiple channels.' )

opts, args = parser.parse_args()

#------------------------

### parse out chanlist config name
if len(args)!=1:
    raise ValueError('please supply exactly 1 input argument\n%s'%usage)
gwf = args[0]

### verbosity
opts.verbose = opts.verbose or opts.Verbose

### set up regex channel exclusion
regex_channel_exclude = [re.compile(_) for _ in opts.regex_channel_exclude]

### ensure lists are unique and sorted
opts.plot_frequency_range = sorted(set(opts.plot_frequency_range))
opts.plot_time_range      = sorted(set(opts.plot_time_range))

### check --q-low to ensure it is acceptable
opts.q_low = ceil(opts.q_low, d=6) ### take the ceilng of q a the 6th decimal place 
                                   ### this ensures Omega will read the correct number from the config file
assert opts.q_low >= sqrt11, '--q-low=%.6f < sqrt(11) and will not be accepted by Omega'%opts.q_low

#-------------------------------------------------

### compute distance in parameter-space corresponding to --max-engery-loss
### used to determine Omega's tiling and therefore the acceptable frequency ranges
ds = 2 * (opts.max_energy_loss/3.)**0.5

#-------------------------------------------------

### read in channels from frame

if opts.verbose:
    print( 'reading channels from : %s'%gwf )

channels = gwf2chans( gwf )

if opts.Verbose:
    for chan, fs in channels.items():
        print "    %8.1f\t%s"%(fs, chan)

### remove channels we don't want
for chan in opts.channel_exclude:
    #     it was in channels          we're printing a lot of stuff
    if channels.pop(chan, None) and opts.Verbose:
        print( 'excluding : %s'%chan )

for chan in channels.keys():
    for regex in regex_channel_exclude:
        if regex.match(chan):
            channels.pop(chan)
            if opts.Verbose:
                print( 'excluding : %s'%chan )

#------------------------

### extract frame type
frameType = os.path.basename( gwf ).split('-')[1] ### follows standard ligo convention

### figure out Omega's q tiling
### this is taken from lines 252-275 of https://svn.ligo.caltech.edu/svn/omega/trunk/src/wtile.m
Qratio = opts.q_high/opts.q_low
nQ     = np.ceil( np.log( Qratio ) / (sqrt2*ds) ) ### the number of Q-tiles Omega will use

minQ   = opts.q_low * ( Qratio )**(+0.5/nQ)       ### the smallest Q-tile Omega will use
maxQ   = opts.q_high * ( Qratio )**(-0.5/nQ)      ### the biggest Q-tile Omega will use

#-------------------------------------------------

### set up OmegaScan config as a string
omegascan = """# Q Scan configuration file
# Automatically generated with %(prog)s
# by %(username)s on %(date)s
# from %(gwframe)s

# gwf-to-omegascan.py %(gwframe)s \\"""%{
     'prog'     : __file__,
     'username' : getpass.getuser(),
     'date'     : time.strftime("%Y-%m-%d %H:%M:%S"),
     'gwframe'  : gwf,
    }

### report the command line options
for option, value in vars(opts).items():
    option = option.replace('_','-') ### make this look like a command-line option
    if isinstance(value, list):
        for val in value:
            omegascan += "\n#   --%s=%s \\"%(option, val)
    elif isinstance(value, bool):
        if value: ### only print if this is True -> it was supplied
            omegascan += "\n#   --%s \\"%(option)
    elif value!=None: ### something was supplied, so this isn't a default
        omegascan += "\n#   --%s=%s \\"%(option, value)

### print common sections
omegascan += """

[Context, Context]

[%(frameType)s, %(frameType)s]"""%{'frameType':frameType}

#-------------------------------------------------
### iterate over channels
#-------------------------------------------------

for channel in sorted(channels.keys()):
    fsamp = channels[channel]

    if opts.Verbose:
        print( "    channel : %s"%channel )

    #-----------------------------------------

    ### figure out the maximum frequency
    ### definition of largest alloweable frequency comes from line 285 in https://svn.ligo.caltech.edu/svn/omega/trunk/src/wtile.m
    ### we do this here becuase other parameters may depend on it
    fmax = opts.f_high if opts.f_high!=None \
               else floor( fsamp/(2*(1+sqrt11/minQ)), d=6 )
#          from options     if supplied
#                   floor of max allowed by Omega (to avoid aliasing) at 6th decimal place (to ensure Omega reads the right value)

    ### ensure plot_time_range does not conflict with search_time_range
    ### this ensures that searchTimeRange is big enough if the user has supplied --plot-time-range
    plotTimeRange = opts.plot_time_range if opts.plot_time_range \
                        else plot_time_range( fmax )

    if opts.search_time_range==None: ### not supplied, so we need to make sure it's compatible with plotTimeRange
        searchTimeRange = max( search_time_range( fmax ), 2*plotTimeRange[-1] ) ### plotTimeRange is known to be sorted
                                                                                ### plotTimeRange[-1] = max(plotTimeRange)
    else: ### this was supplied, so just use that *without* checking
        searchTimeRange = opts.search_time_range

    ### figure out low frequency bound
    ### we must do this here because it depends on searchTimeRange, which may depend on fmax, which may be different for each channel
    if opts.f_low==None: ### not supplied, so extract from INI and compare to what Omega will allow
        #  minimum allowed by Omega (to get enough tiles for good statistics)
        flow = ceil( 50*maxQ / (twopi * searchTimeRange) , d=6 ) ### take ceiling at 6th decimal place
                                                                                                              ### ensures Omega will read the correct value
        # definition of lowest alloweable frequency comes from line 281 in https://svn.ligo.caltech.edu/svn/omega/trunk/src/wtile.m
    else: ### was supplied, so we just use this *without* checking
        flow = opts.f_low

    ### parse the channel into a string for OmegaScan config
    omegascan += """
{
  channelName:               '%(channel)s'
  frameType:                 '%(frameType)s'
  sampleFrequency:           %(fsamp).6f
  searchTimeRange:           %(searchWin).6f
  searchFrequencyRange:      [%(flow).6f %(fhigh).6f]
  searchQRange:              [%(qlow).6f %(qhigh).6f]
  searchMaximumEnergyLoss:   %(minEloss).6f
  whiteNoiseFalseRate:       %(wnFAR).6e
  searchWindowDuration:      %(win).6f
  plotTimeRanges:            [%(plt_trange)s]
  plotFrequencyRange:        [%(plt_frange)s]
  plotNormalizedEnergyRange: [%(plt_Emin).6f %(plt_Emax).6f]
  alwaysPlotFlag:            %(alwaysPlt)d
}"""%{
     'channel'    : channel,                                                    ### from this channel
     'frameType'  : frameType,                                                  ### from section 
     'fsamp'      : fsamp,                                                      ### from this channel
     'searchWin'  : searchTimeRange,                                            ###     SET WITH OPTIONS
     'win'        : opts.search_window_duration if opts.search_window_duration!=None \
                        else search_window_duration( fmax ),
                                                                                ###     SET WITH OPTIONS
     'flow'       : flow,                                                       ### from section or OPTIONS (if supplied)
     'fhigh'      : fmax,                                                       ### from section and/or this channel or OPTIONS
     'qlow'       : opts.q_low,                                                 ###     SET WITH OPTIONS
     'qhigh'      : opts.q_high,                                                ### from section or OPTIONS (if supplied)
     'minEloss'   : opts.max_energy_loss,                                       ###     SET WITH OPTIONS
     'wnFAR'      : opts.white_noise_false_alarm_rate,                          ###     SET WITH OPTIONS
     'plt_trange' : " ".join( "%.1f"%_ for _ in plotTimeRange ),                ###     SET WITH OPTIONS
     'plt_frange' : " ".join( "%.1f"%_ for _ in opts.plot_frequency_range ),    ###     SET WITH OPTIONS
     'plt_Emin'   : opts.plot_energy_min,                                       ###     SET WITH OPTIONS
     'plt_Emax'   : opts.plot_energy_max,                                       ###     SET WITH OPTIONS
     'alwaysPlt'  : opts.always_plot_all_channels \
                        or (channel in opts.always_plot_channel),               ###     SET WITH OPTIONS
    }

#-------------------------------------------------

### write out the config file
if opts.output_file:
    if opts.verbose:
        print( "writing OmegaScan config to : %s"%opts.output_file )
    file_obj = open(opts.output_file, 'w')
    file_obj.write( omegascan ) ### NOTE: this way sorta needlessly uses more memory than necessary because I don't every need the full string at once...
    file_obj.close()

else:
    print >> sys.stdout, omegascan
